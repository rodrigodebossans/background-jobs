import { Router } from 'express';
import UserController from '@controllers/UserController';

const router: Router = Router();

router.route('/user').post(UserController.addUser);

export const UserRoutes: Router = router;