import Mail from '@config/mail';
import { Schedulable } from '@interfaces/Job';

export class RegistrationMailJob implements Schedulable {
  public key: string = 'RegistrationMail';

  async execute({ data }): Promise<void> {
    const { user } = data;
    await Mail.sendMail({
      from: 'Bússola Queue <rodrigodebossans@gmail.com>',
      to: `${user.name}, <${user.email}>`,
      subject: 'Queue info',
      html: `Olá ${user.name}, bem vindo ao novo sistema de filas!`,
    });
  }
}

export default new RegistrationMailJob();