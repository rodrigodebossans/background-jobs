import { Request, Response } from 'express';
import { User } from '@models/User';
import UserService from '@services/UserService';
import { HttpStatusCodesEnum } from '@enums/HttpStatusCodesEnum';

class UserController {
  async addUser(req: Request, res: Response): Promise<Response> {
    try {
      const user: User = User.fromUser(req.body);
      User.validate(user);
      const newUser: User = await UserService.addUser(user);

      return res.status(HttpStatusCodesEnum.OK).json(newUser);
    } catch (error) {
      console.error(error);
      return res.status(error?.httpStatusCode).json(error);
    }
  }
}

export default new UserController();