import { QueueOptions } from 'bull';

const queueOpts: QueueOptions = {
  redis: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
  },
};

export default queueOpts;