import Queue, { JobOptions } from 'bull';
import RedisConfig from '@config/redis';

import * as jobs from '@jobs/index';

const queues = Object.values(jobs).map((job) => ({
  bull: new Queue(job.key, RedisConfig),
  name: job.key,
  execute: job.execute,
}));

export default {
  queues,
  add(name, data, opts: JobOptions) {
    const queue = this.queues.find((q) => q.name === name);

    return queue.bull.add(data, opts);
  },
  process() {
    return this.queues.forEach((q) => {
      q.bull.process(q.execute);

      q.bull.on('failed', (job) => {
        console.log('Job Failed', job.name, job.data);
      });
    });
  },
};