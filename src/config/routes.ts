import { UserRoutes } from '@routes/UserRoute';
import { createBullBoard } from 'bull-board';
import { BullAdapter } from 'bull-board/bullAdapter';
import Queue from '@config/queue';

const { router } = createBullBoard(
  Queue.queues.map((queue) => new BullAdapter(queue.bull)),
);

const API = '/api';

export default (app) => {
  app.use('/jobs', router);
  app.use(API, UserRoutes);
};