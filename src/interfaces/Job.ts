export interface Schedulable {
  key: string;
  // eslint-disable-next-line no-unused-vars
  execute(data: any): Promise<void>;
}